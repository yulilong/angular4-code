import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
@Component({
  selector: 'protection-manage',
  templateUrl: './protection-manage.component.html',
  styleUrls: ['./protection-manage.component.scss,./../../product-service.component.scss']
})
export class ProtectionManageComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
  ) { }
  public newIsVisible: boolean = false; // 停用弹出框的状态
  public isVisible: boolean = false; // 更换弹出框的状态
  public openVisible: boolean = false; // 开通弹出框按钮
  public validateForm: FormGroup; // 更换弹出框内容规则
  public opendateForm: FormGroup; // 开通弹出框内容规则
  public searchOptions = [
    { label: '监控类别', value: 'monitorType' },
    { label: '监控对象', value: 'monitorObject' },
    { label: '规则名称', value: 'ruleName' },
    { label: '状态', value: 'alarmHistoryStates' },
    { label: '通知人', value: 'notifyUserName' },
  ];

  ngOnInit() {
    this.validateForm = this.fb.group({
      selectedOption: [null, [Validators.required]],
    });
    this.opendateForm = this.fb.group({
      administrator: [null, [Validators.required]],
    })

  }
  
  /**
   * 
   * 1、
   * 停止应用
   */
  public stop() {
    this.newIsVisible = true;
  };

  /**
* 停用确认
* @param e 
*/
  public stopOk(e) {
    this.newIsVisible = false;
  };
  public stopCancel(e) {
    this.newIsVisible = false;
  }
  /**
   * 2、
 * 更换管理员按钮
 */
  public change() {
    this.isVisible = true;
  };

  /**
  * 更换弹框确认按钮
  * @param e 
  */
  public sendOk(e) {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
    }
    // this.isVisible = false;
  };
  /**
  * 更换取消事件
  * @param e 
  */
  public newhandleCancel($event: MouseEvent) {
    $event.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[key].markAsPristine();
    };
    this.newIsVisible = false;
    this.isVisible = false;
  };

  /**
   * 3、
   * 立即开通
   */
  public opened() {
    this.openVisible = true;
  };

  /**
   * 
   * @param e 开通确认
   */
  public openOk(e) {
    for (const i in this.opendateForm.controls) {
      this.opendateForm.controls[i].markAsDirty();
    }
  };
  /**
   * 开通取消
   * @param  
   */
  public openhandleCancel($event: MouseEvent) {
    $event.preventDefault();
    this.validateForm.reset();
    for (const key in this.opendateForm.controls) {
      this.opendateForm.controls[key].markAsPristine();
    };
    this.openVisible = false;
  };

  public getFormControl(name) {
    return this.validateForm.controls[name];
  };
  public openFormControl(name) {
    return this.opendateForm.controls[name];
  };

}
