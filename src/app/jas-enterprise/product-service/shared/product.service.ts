import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { INCONFIG } from './../../../core/global';
import { RequestMethodService } from '../../../core/services/request-method.service';
@Injectable()
export class ProductService {
    public serviceName = INCONFIG.monitorServerSrc;
    public dataServerSrc = INCONFIG.fileServerSrc;
    constructor(
        public http: Http,
        private requestMethodService: RequestMethodService
    ) { }
    public searchData(__this: any, url: any, value: any, successhandler?: any, errorhandler?: any) {
        let requestUrl = this.serviceName + url;
        this.requestMethodService.doPost(__this, requestUrl, value, successhandler, errorhandler)
    };
}
