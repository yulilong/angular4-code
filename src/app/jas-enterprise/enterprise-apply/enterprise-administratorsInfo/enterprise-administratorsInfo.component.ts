import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
@Component({
  selector: 'enterprise-administratorsInfo',
  templateUrl: './enterprise-administratorsInfo.component.html',
  styleUrls: ['./enterprise-administratorsInfo.component.scss']
})
export class EnterpriseAdministratorsInfoComponent implements OnInit {
  public validateForm: FormGroup; // 编辑的弹出框内容规则
  public identifyStatus: boolean = true; // 原手机号获取验证码按钮的状态
  public sendStatus: boolean = false; // 原手机号发送验证码的状态
  public time: any; // 全局定时器
  public count = 10; // 倒计时辅值
  public disabled: boolean = false; // 原手机号页面获取密码按钮的状态
  public nowIdentifyStatus: boolean = true; // 现手机号获取验证码按钮的状态
  public nowSendStatus: boolean = false; // 现手机号发送验证码的状态

  public current = 0;
  public firstContent = true;
  public secondContent: boolean;
  public thirdContent: boolean;
  public fourContent: boolean;
  // 步骤一
  public oneIsActive = true;
  public TwoisActive = true;
  // 步骤二
  public threeIsActive: boolean;
  public fourIsActive: boolean;
  public fiveIsActive: boolean;
  public sixIsActive: boolean;
  // 步骤四 
  elevenIsActive: boolean;
  twelveIsActive: boolean;
  thirteenIsActive: boolean;
  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.validateForm = this.fb.group({
      userName: [1333333333, [Validators.required]],
      phoneNumber: [null, null],
      identifyingCode: [null, null]
    });
  }
  // pre() {
  //   this.current -= 1;
  //   this.changeContent();
  // };
  /**
   * 下一步
   */
  next() {
    this.current += 1;
    this.changeContent();
  };
  // done() {

  //   this.current += 1;
  //   this.changeContent();
  // };
  /**
   * 识别按钮
   */
  changeContent() {
    switch (this.current) {
      case 0: {
        this.firstContent = true;
        this.secondContent = false;
        this.thirdContent = false;
        this.fourContent = false;
        this.oneIsActive = true;
        this.TwoisActive = true;
        this.threeIsActive = false;
        this.fourIsActive = false;
        this.fiveIsActive = false;
        this.sixIsActive = false;
        this.elevenIsActive = false;
        this.twelveIsActive = false;
        this.thirteenIsActive = false;
        break;
      }
      case 1: {
        this.firstContent = false;
        this.secondContent = true;
        this.thirdContent = false;
        this.fourContent = false;
        this.oneIsActive = false;
        this.TwoisActive = false;
        this.threeIsActive = true;
        this.fourIsActive = true;
        this.fiveIsActive = true;
        this.sixIsActive = true;
        this.elevenIsActive = false;
        this.twelveIsActive = false;
        this.thirteenIsActive = false;
        break;
      }

      case 2: {
        this.firstContent = false;
        this.secondContent = false;
        this.thirdContent = false;
        this.fourContent = true;
        this.oneIsActive = false;
        this.TwoisActive = false;
        this.threeIsActive = false;
        this.fourIsActive = false;
        this.fiveIsActive = false;
        this.sixIsActive = false;
        this.elevenIsActive = true;
        this.twelveIsActive = true;
        this.thirteenIsActive = true;
        break;
      }
      default: {
        this.firstContent = false;
      }
    }
  }
  public getFormControl(name) {
    return this.validateForm.controls[name];
  };


  /**
   *原手机号获取验证码的点击事件
   */

  public identifyClick() {
    this.sendStatus = true; // 远手机号获取验证码的状态
    this.identifyStatus = false; // 可重发的显示的状态
    this.disabled = true; // 按钮禁止状态
    this.timer();   // 调用全局定时器
    this.count = 10; // 重新给全局变量辅值
  };


  /**
 * 设置全局定时器
 */
  public timer() {
    this.time = setInterval(() => {
      if (this.count <= 0) {
        this.disabled = false; // 原手机号的按钮的状态
        this.sendStatus = false; // 远手机号获取验证码的状态
        this.nowIdentifyStatus = true; // 可重发的显示的状态
        this.identifyStatus = true;  // 原手机号获取验证码按钮的状态
        this.nowSendStatus = false;// 现手机号发送验证码的状态
        return clearInterval(this.time); // 清除定时器
      } else {
        this.count--;
      }
    }, 1000);
  }
}
