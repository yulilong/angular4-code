import { Routes, RouterModule } from '@angular/router';
import { EnterpriseApplyComponent } from './enterprise-apply.component';
import { EnterpriseSummaryComponent } from './enterprise-summary/enterprise-summary.component';
import { EnterpriseUserInfoComponent } from './enterprise-userInfo/enterprise-userInfo.component';
import { EnterpriseRoleInfoComponent } from './enterprise-roleInfo/enterprise-roleInfo.component';
import { EnterpriseExamineInfoComponent } from './enterprise-examineInfo/enterprise-examineInfo.component';
import { EnterpriseAdministratorsInfoComponent } from './enterprise-administratorsInfo/enterprise-administratorsInfo.component';
const routes: Routes = [
    {
        path: '',
        component: EnterpriseApplyComponent,
        children: [
            { path: '', redirectTo: 'enterprise-summary', pathMatch: 'full' },
            {
                path: 'enterprise-summary',
                component: EnterpriseSummaryComponent
            },
            {
                path: 'enterprise-userInfo',
                component: EnterpriseUserInfoComponent
            },
            {
                path: 'enterprise-examineInfo',
                component: EnterpriseExamineInfoComponent
            },
            {
                path: 'enterprise-administratorsInfo',
                component: EnterpriseAdministratorsInfoComponent
            },
            {
                path: 'enterprise-roleInfo',
                component: EnterpriseRoleInfoComponent
            }

        ]
    }


]
export const EnterpriseApplyRoutes = RouterModule.forChild(routes)