import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
@Component({
  selector: 'enterprise-examineInfo',
  templateUrl: './enterprise-examineInfo.component.html',
  styleUrls: ['./enterprise-examineInfo.component.scss']
})
export class EnterpriseExamineInfoComponent implements OnInit {
  _dataSet = []; // 列表数据
  public validateForm: FormGroup; // 编辑的弹出框内容规则
  public newVisible: boolean = false; // 弹窗的状态
  public searchOptions = [
    { label: '通过', value: 'monitorType' },
    { label: '驳回', value: 'monitorObject' },

  ];
  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.validateForm = this.fb.group({
      selectedOption: [null, [Validators.required]],
      applyValue: [null, null]
    });
    for (let i = 0; i < 5; i++) {
      // for (let i = 0; i < 0; i++) {
      this._dataSet.push({
        key: i,
        name: `兽人永不为奴${i}`,
        age: 32,
        address: `London, Park Lane no. ${i}`,
        number: i + 1,
        colorName: '红色' + i + 10,
        oracle: 'oracle----01928392981----' + i + 100,
      });
    };
  };
  public getFormControl(name) {
    return this.validateForm.controls[name];
  };
  /**
   * 弹出事件
   */
  public clickMessage() {
    this.newVisible = true;
  }

  /**
* 弹出框取消事件
* @param e 
*/
  public newCancel($event: MouseEvent) {
    $event.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[key].markAsPristine();
    };
    this.newVisible = false;

  };
  /**
* 更换弹框确认按钮
* @param e 
*/
  public sendOk(e) {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
    }
    // this.newVisible = false;
  };

}
