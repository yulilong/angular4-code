import { Component, OnInit, OnChanges } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
@Component({
  selector: 'enterprise-apply',
  templateUrl: './enterprise-apply.component.html',
  styleUrls: ['./enterprise-apply.component.scss']
})
export class EnterpriseApplyComponent implements OnInit {
  tab = true;
  public summaryState: any; //概述
  public userState: any; //用户
  public roleState: any; // 角色
  public examineState: any; //审核
  public AdministratorsState: any; //更换管理员
  public value: any;
  constructor(
    private router: Router,
    public route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.summaryState = this.router.url.indexOf('enterprise-summary');
    this.userState = this.router.url.indexOf('enterprise-userInfo');
    this.roleState = this.router.url.indexOf('ernterprise-roleInfo');
    this.examineState = this.router.url.indexOf('enterprise-examineInfo');
    this.AdministratorsState = this.router.url.indexOf('enterprise-administratorsInfo');
    this.value = this.route.queryParamMap['value'] ? this.route.queryParamMap['value'] : this.route.paramMap['value'];


    console.log(this.route.queryParamMap)
    console.log(this.route.paramMap)
    // this.route.paramMap.subscribe((params: ParamMap) => {
    //   console.log(params)
    //   // if (params['enterprise-examineInfo']) {
    //   //   console.log(11111)
    //   //   this.summaryState = -1;
    //   //   this.userState = -1;
    //   //   this.roleState = -1;
    //   //   this.examineState = 1;
    //   //   this.AdministratorsState = -1;
    //   // }
    // });
    this.value = this.route.paramMap
      .switchMap((params: ParamMap) =>
        (params.get('value')));

    console.log(this.value)

  };
  // ngOnChanges() {
  //   this.route.queryParams.subscribe((params: Params) => {
  //     console.log(this.examineState)
  //     if (params['enterprise-examineInfo']) {
  //       console.log(11111)
  //       this.summaryState = -1;
  //       this.userState = -1;
  //       this.roleState = -1;
  //       this.examineState = 1;
  //       this.AdministratorsState = -1;
  //     }
  //   });
  // }
  /**
   * 概述
   */
  public summaryInfo() {
    this.summaryState = 1;
    this.userState = -1;
    this.roleState = -1;
    this.examineState = -1;
    this.AdministratorsState = -1;
    const urls = this.router.url.slice(0, this.router.url.indexOf('enterprise-apply')) + 'enterprise-apply/enterprise-summary';
    this.router.navigate([urls]);
  };
  /**
   * 用户
   */
  public userInfo() {
    this.summaryState = -1;
    this.userState = 1;
    this.roleState = -1;
    this.examineState = -1;
    this.AdministratorsState = -1;
    const urls = this.router.url.slice(0, this.router.url.indexOf('enterprise-apply')) + 'enterprise-apply/enterprise-userInfo';
    this.router.navigate([urls]);
  };
  /**
   * 角色
   */
  public roleInfo() {
    this.summaryState = -1;
    this.userState = -1;
    this.roleState = 1;
    this.examineState = -1;
    this.AdministratorsState = -1;
    const urls = this.router.url.slice(0, this.router.url.indexOf('enterprise-apply')) + 'enterprise-apply/enterprise-roleInfo';
    this.router.navigate([urls]);
  };
  /**
   * 审核
   */
  public examineInfo() {
    this.summaryState = -1;
    this.userState = -1;
    this.roleState = -1;
    this.examineState = 1;
    this.AdministratorsState = -1;
    const urls = this.router.url.slice(0, this.router.url.indexOf('enterprise-apply')) + 'enterprise-apply/enterprise-examineInfo';
    this.router.navigate([urls]);
  };
  /**
   * 更换管理员
   */
  public AdministratorsInfo() {
    this.summaryState = -1;
    this.userState = -1;
    this.roleState = -1;
    this.examineState = -1;
    this.AdministratorsState = 1;
    const urls = this.router.url.slice(0, this.router.url.indexOf('enterprise-apply')) + 'enterprise-apply/enterprise-administratorsInfo';
    this.router.navigate([urls]);
  };

}
