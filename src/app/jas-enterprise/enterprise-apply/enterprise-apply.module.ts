import { NgZorroAntdModule } from 'ng-zorro-antd';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnterpriseApplyComponent } from './enterprise-apply.component';
import { EnterpriseApplyRoutes } from './enterprise-apply.routing';
import { EnterpriseSummaryComponent } from './enterprise-summary/enterprise-summary.component';
import { EnterpriseUserInfoComponent } from './enterprise-userInfo/enterprise-userInfo.component';
import { EnterpriseRoleInfoComponent } from './enterprise-roleInfo/enterprise-roleInfo.component';
import { EnterpriseExamineInfoComponent } from './enterprise-examineInfo/enterprise-examineInfo.component';
import { EnterpriseAdministratorsInfoComponent } from './enterprise-administratorsInfo/enterprise-administratorsInfo.component';
import { CommonService } from './../../core/services/conmmon.service';
import { NgaModule } from './../../theme/nga.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({

    imports: [
        CommonModule,
        FormsModule,
        NgZorroAntdModule.forRoot(),
        EnterpriseApplyRoutes,
        NgaModule,
        ReactiveFormsModule
    ],
    declarations: [
        EnterpriseApplyComponent, EnterpriseSummaryComponent, EnterpriseUserInfoComponent,
        EnterpriseExamineInfoComponent, EnterpriseAdministratorsInfoComponent, EnterpriseRoleInfoComponent
    ],
    providers: [CommonService]
})
export class EnterpriseApplyModule { }
