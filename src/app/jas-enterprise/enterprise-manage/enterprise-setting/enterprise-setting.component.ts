import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';    // NG-ZORRO Form表单需要的

@Component({
  selector: 'app-enterprise-setting',
  templateUrl: './enterprise-setting.component.html',
  styleUrls: ['./enterprise-setting.component.scss']
})
export class EnterpriseSettingComponent implements OnInit {

  public enterpriseInfo: any;             // 企业基本信息
  public showAddImgDialog = false;        // 显示添加图片弹窗 显示与隐藏
  public pageSection = 1;                 // 页面显示部分： 1:企业信息部分  2：认证介绍页面 3：企业认证部分
  public entCertForm: FormGroup;          // 输入企业认证信息表单
  public entLogoImg: any = null;          // 图片： 企业logo图标
  public entBusinessLiceImg: any = null;  // 图片： 企业 营业执照照片
  public identityCardImg: any = null;     // 图片： 法人身份证照片
  public addWhereImg: any;                // 添加哪个部分的图片： 1：企业logo   2：营业执照  3：法人身份证
  constructor(
    private fb: FormBuilder,                    // NG-ZORRO 需要的Form数据格式
  ) { }

  @ViewChild('entSettingDiv') entSettingDiv;    // 组件的HTML
  @ViewChild('ngxImg') ngxImg;                  // 上传图片组件对象
  ngOnInit() {
    // 网页头部60px window.innerHeight: 网页中 body元素的高度
    this.entSettingDiv.nativeElement.style.minHeight = window.innerHeight - 60 + 'px';

    this.enterpriseInfo = {
      enterpriseName: '淘宝',
      enterpriseScale: '100-200人',
      telephoneNum: '15601361080',
      location: null,
      address: null,
      certification: '未认证',
      auditInfo: null,
    };
    // 企业认证信息表单初始化
    this.entCertForm = this.fb.group({
      entName        : [null, [ this.SectionNameValidator ]],
      registerNumber : [null, [Validators.required]],
      businessLicense: [null, [Validators.required]],
      identityCard   : [null, [Validators.required]],
      verify         : false, // 确认是否 全部填写了
    });
  }

  /**
   * 选择图片后，获取图片数据
   * @param {event} 页面事件
   * @param {where} 添加的是哪个部分的图片：1：企业logo   2：营业执照  3：法人身份证
   * @memberof EnterpriseSettingComponent
   */
  public onSelect(event: any, where) {
    if (where === 1) {
      this.entLogoImg = event;
    } else if (where === 2) {
      this.entBusinessLiceImg = event;
    } else if (where === 3) {
      this.identityCardImg = event;
    }
  }

  /**
   * 清除选择的图片数据
   * @param {where} 添加的是哪个部分的图片：1：企业logo   2：营业执照  3：法人身份证
   * @memberof EnterpriseSettingComponent
   */
  reset(where) {
    if (where === 1) {
      this.entLogoImg = null;
    } else if (where === 2) {
      this.entBusinessLiceImg = null;
    } else if (where === 3) {
      this.identityCardImg = null;
    }
  }

  /**
   * 上传图片按钮 处理方法
   * @param {where} 添加的是哪个部分的图片：1：企业logo   2：营业执照  3：法人身份证
   * @memberof EnterpriseSettingComponent
   */
  public uploadImgBtn(where) {
    this.showAddImgDialog = true;       // 显示上传图片弹窗
    if (this.ngxImg) {
      this.addWhereImg = 1000;          // 随机设置一个值，以便于在 上传组件初始化的时候，不会把之前的图片删除
      this.ngxImg.reset();              // 清除 上传组件中遗留的数据
    }
    this.addWhereImg = where;           // 设置上传的是哪个图片
  }

  /**
   * 提交审核按钮方法
   * @param {where} 添加的是哪个部分的图片：1：企业logo   2：营业执照  3：法人身份证
   * @memberof EnterpriseSettingComponent
   */
  public submitAuditBtn() {

    if (this.entCertForm.valid) {
      this.entCertForm.value.verify = false;
    } else {
      this.entCertForm.value.verify = true;
    }
    this.enterpriseInfo.auditInfo = '认证申请已提交成功，正在审核中...';
    console.log('填写的数据：', this.entCertForm.value);
    // pageSection = 3;
  }

  /**
   * 名字格式验证： 中文、字母、数字、“-”、“_”，1-20个字符
   * @param {control} 表单数据
   * @memberof SectionUserManageComponent
   */
  public SectionNameValidator(control: FormControl): { [s: string]: boolean } {
    const EMAIL_REGEXP = /^([A-Za-z0-9_\u4E00-\u9FA5]|-)+$/;
    if (!control.value) {
      return { required: true };
    } else if (!EMAIL_REGEXP.test(control.value)) {
      return { error: true, name: true };
    }
  }
}
