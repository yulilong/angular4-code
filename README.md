# cloudlink-plat-enterprise

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.6.

## 一、项目的执行命令

### 1、启动项目

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### 2、生成代码

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### 3、生成生产文件

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## 二、项目的目录结构

```
.
├── README.md
├── karma.conf.js
├── package-lock.json
├── package.json            // 依赖包配置文件
├── protractor.conf.js     
├── proxy.conf.json         // 代理配置解决跨域请求配置文件
├── src                     // 根目录
│   ├── app                  
│   │   ├── app.component.css
│   │   ├── app.component.html
│   │   ├── app.component.spec.ts
│   │   ├── app.module.ts
│   │   ├── app.routing.ts  // 入口模块的路由配置文件
│   │   ├── auth            // 注册，验证文件夹
│   │   ├── core            // 核心文件夹
│   │   ├── dashboard       // 仪表盘
│   │   └── theme           // 主题文件夹
│   │       ├── component   // 组件类文件夹
│   │       ├── directive   // 指令类文件夹
│   │       ├── pipes       // 管道类文件夹
│   │       ├── sass        // 样式类文件夹
│   │       └── services    // 服务类文件夹
│   ├── assets              // 静态资源的文件夹
│   │   ├── icon-font       // 字体图标文件
│   │   └── img             // 图片文件
│   ├── environments        // 运行环境文件
│   │   ├── environment.prod.ts  // 生产环境
│   │   └── environment.ts       // 开发环境
│   ├── index.html          // 主页面
│   ├── main.ts
│   ├── polyfills.ts        // 填充物文件
│   ├── styles.scss         // 全局样式文件
│   ├── test.ts
│   ├── tsconfig.app.json
│   ├── tsconfig.spec.json
│   └── typings.d.ts
├── tsconfig.json
└── tslint.json

```
